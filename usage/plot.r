for LA in pl java Cs C JS PY R
do ls -f L$LA.* | sed "s|L$LA\.||" | while read i; do zcat L$LA.$i|cut -d\; -f2 | sort | uniq -c | awk '{print "'$i';"$2";"$1}'; done > cnt.L$LA
done


for (LA in c("pl", "Cs", "JS")){

for (LA in c("R")){

y = read.table(paste("cnt.L",LA,sep=""), sep=";",col.names=c("m","y","c"));
ll = -sort (-tapply(y$c,y$m,max))

for (prt in 0:2){
  png(filename=paste(c(LA,"-modules.",prt,".png"), collapse=""),width=900,height=650);
  rng = (1:10)+prt*10;
  first = 1;
  N=10;
  for (m in names(ll)[rng]){
    x = y[y$m==m,];
    x=x[x$y>2012&x$y<2019,];
    x=x[-dim(x)[1],];
    x$c[x$c < 10]=NA;
    if (first==1){
      mx = max(x$c,na.rm=T);
      mn = mx/100;
      if (mn <100) mn = 100;
      plot (x$y, x$c, ylim=c(mn,mx),main=paste("Module trends for", LA),
              xlab="Years", ylab="New projects per month",
              type="o", lwd=2, lty=1,col=1,log="y");
    }else{
        lines(x$y, x$c,col=first,lty=first,lwd=2);
    }
    first = first +1;
  }
  legend(2017.1,min(mn*3,mx-mn),legend=names(ll)[rng],lty=1:N,col=1:N,lwd=2,bg="white")
  dev.off();
}  

}


